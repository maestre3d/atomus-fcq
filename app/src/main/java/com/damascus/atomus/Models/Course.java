package com.damascus.atomus.Models;

import java.util.ArrayList;

public class Course {
    private String name, docent, grade;

    public Course(String name, String docent, String grade){
        this.name = name;
        this.docent = docent;
        this.grade = grade;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDocent(String docent) {
        this.docent = docent;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public String getDocent() {
        return docent;
    }

    public String getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return this.name + " " + this.docent + " " + this.grade;
    }


    private static int lastCourseId = 0;
    public static ArrayList<Course> createCourseList(int x){
        ArrayList<Course> courses = new ArrayList<Course>();

        for(int i = 1; i <= x; i++){
            courses.add(new Course("Quimica  " + ++lastCourseId, "Juan Carlos Acosta Vazquez", x + " Semestre"));
        }

        return courses;

    }
}
