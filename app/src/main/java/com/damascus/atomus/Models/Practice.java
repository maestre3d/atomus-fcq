package com.damascus.atomus.Models;

import java.util.ArrayList;

public class Practice {
    public String name;
    public String expDate;
    public String courseName;
    public String labName;

    public Practice(String pName, String cName){
        name = pName;
        courseName = cName;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getName() {
        return name;
    }

    public String getExpDate() {
        return expDate;
    }

    public String getLabName() {
        return labName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    private static int lastPracticeId = 0;
    public static ArrayList<Practice> createPracticesList(int numPractices){
        ArrayList<Practice> practices = new ArrayList<Practice>();

        for(int i = 1; i <= numPractices; i++){
            practices.add(new Practice("Practica " + ++lastPracticeId, "Quimica Loca I"));
        }

        return practices;

    }
}
