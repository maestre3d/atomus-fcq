package com.damascus.atomus.Models;

public class Order {
    public String expDate;
    public String reqDate;
    public String practice;
    public String[] materials = new String[5];

    public Order(String expDate, String reqDate, String practice){
        this.expDate = expDate;
        this.reqDate = reqDate;
        this.practice = practice;
        setMaterials();
    }

    public String getExpDate() {
        return expDate;
    }

    public String getPractice() {
        return practice;
    }

    public String getReqDate() {
        return reqDate;
    }

    public String[] getMaterials() {
        return materials;
    }

    private void setMaterials(){
        for(int i = 0; i < materials.length; i++){
            materials[i] = "Matraz " + i;
        }
    }
}
