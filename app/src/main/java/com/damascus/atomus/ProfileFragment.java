package com.damascus.atomus;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.damascus.atomus.Adapters.ProfileAdapter;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    Button btnLogout;
    Button btnBrowser;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set window name
        CustomGlobal.window = "PROFILE";
        CustomGlobal.isHome = true;

        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        // Get recycler from R
        RecyclerView rvProfile = (RecyclerView) v.findViewById(R.id.profile_list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        ProfileAdapter adapter = new ProfileAdapter(CustomGlobal.courses);
        rvProfile.setHasFixedSize(true);

        rvProfile.setLayoutManager(linearLayoutManager);
        rvProfile.setAdapter(adapter);

        // Declare components
        btnLogout = (Button) v.findViewById(R.id.profile_logut);
        btnBrowser = (Button) v.findViewById(R.id.profile_bt1);

        btnLogout.setOnClickListener(this);
        btnBrowser.setOnClickListener(this);


        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.profile_logut:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);

                builder.setMessage("Para cerrar su sesión, favor de presionar el botón Aceptar.")
                        .setTitle("¿Desea cerrar sesión?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
                break;
            case R.id.profile_bt1:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://fcq.uach.mx/"));
                startActivity(browserIntent);
                break;
        }
    }


}
