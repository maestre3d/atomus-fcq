package com.damascus.atomus.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.damascus.atomus.Models.Practice;
import com.damascus.atomus.PreorderActivity;
import com.damascus.atomus.R;

import java.util.List;

public class HomeAdapter extends
        RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView courseN;
        public TextView practiceN;
        public Button rvButton;


        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            courseN = (TextView) itemView.findViewById(R.id.adap_h_courseN);
            practiceN = (TextView) itemView.findViewById(R.id.adap_h_prac);
            rvButton = (Button) itemView.findViewById(R.id.adap_h_btn1);
        }

    }

    private List<Practice> mPractice;
    PreorderActivity preorderActivity = new PreorderActivity();

    public HomeAdapter(List<Practice> practices){
        mPractice = practices;
    }

    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate custom layout
        View practiceView = inflater.inflate(R.layout.home_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(practiceView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder viewHolder, int position){
        // Get data model based on position
        final Practice practice = mPractice.get(position);

        // Set item based on views and data model
        TextView textView = viewHolder.practiceN;
        textView.setText(practice.getName());
        TextView textView1 = viewHolder.courseN;
        textView1.setText(practice.getCourseName());
        Button bt1 = (Button) viewHolder.rvButton;

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), PreorderActivity.class);
                preorderActivity.setCourse(practice.getCourseName());
                preorderActivity.setPractice(practice.getName());
                v.getContext().startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount(){
        return mPractice.size();
    }
}
