package com.damascus.atomus;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import com.damascus.atomus.Models.Practice;

import java.util.ArrayList;


public class OrderFragment extends Fragment {

    public ListView listView;
    ListView listView2;
    OrderActivity orderActivity = new OrderActivity();
    private boolean flag = false;

    // Genarate an string array
    final ArrayList<Practice> practices = new ArrayList<Practice>();


    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set window name
        CustomGlobal.window = "ORDER";

        // Get View to return and inflate
        View v = inflater.inflate(R.layout.fragment_order, container, false);


        // Get listviews from R
        listView = (ListView) v.findViewById(R.id.order_list2);
        listView2 = (ListView) v.findViewById(R.id.order_list);

        createPractices();

        // Insert values and create adapter with android default list layour
        final ArrayAdapter<Practice> adapter = new ArrayAdapter<Practice>(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, practices);

        // Set adapters to listviews
        listView.setAdapter(adapter);
        listView2.setAdapter(adapter);

        // Set listener on 1st listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), values[position] +"", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity(), OrderActivity.class);
                orderActivity.setPractice(practices.get(position).getName());
                orderActivity.setTitle(practices.get(position).getCourseName());
                orderActivity.setCourse(practices.get(position).getCourseName());
                getActivity().startActivity(i);
            }
        });
        // Set listener on 2nd listview
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), values[position] +"", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity(), OrderActivity.class);
                orderActivity.setPractice(practices.get(position).getName());
                orderActivity.setTitle(practices.get(position).getCourseName());
                orderActivity.setCourse(practices.get(position).getCourseName());
                getActivity().startActivity(i);
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    private void createPractices(){
        if (flag == false) {
            for( int i = 1; i < 5; i++ ){
                practices.add(new Practice("Practica " + i, "Quimica " + i));
            }
            flag = true;
        }
    }

}
