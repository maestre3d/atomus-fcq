package com.damascus.atomus;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


public class CalendarFragment extends Fragment implements View.OnClickListener {

    ImageButton btnCalendar;


    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set window name
        CustomGlobal.window = "CALENDAR";

        View v = inflater.inflate(R.layout.fragment_calendar, container, false);

        btnCalendar = (ImageButton) v.findViewById(R.id.calendar_bt1);
        btnCalendar.setOnClickListener(this);



        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.calendar_bt1:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://lambdax.000webhostapp.com/assets/img/calendarjan19.png"));
                startActivity(intent);
                break;
        }
    }

}
