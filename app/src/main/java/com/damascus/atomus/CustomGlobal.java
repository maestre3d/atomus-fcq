package com.damascus.atomus;

import com.damascus.atomus.Models.Course;
import com.damascus.atomus.Models.Practice;

import java.util.ArrayList;

public class CustomGlobal {
    public static String window = "HOME";
    public static Boolean isHome = false;
    public static boolean flag = false;
    public static ArrayList<Practice> practices = Practice.createPracticesList(5);
    public static ArrayList<Course> courses = Course.createCourseList(4);
}
