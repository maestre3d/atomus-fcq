package com.damascus.atomus;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {

    // Navigation
    BottomNavigationView navigation;

    // Fragments
    HomeFragment homeFragment;
    OrderFragment orderFragment;
    ProfileFragment profileFragment;
    CalendarFragment calendarFragment;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    CustomGlobal.window = "HOME";
                    setFragment(homeFragment);
                    return true;

                case R.id.navigation_orders:
                    CustomGlobal.window = "ORDERS";
                    setFragment(orderFragment);
                    return true;

                case R.id.navigation_calendar:
                    CustomGlobal.window = "CALENDAR";
                    setFragment(calendarFragment);
                    return true;

                case R.id.navigation_profile:
                    CustomGlobal.window = "PROFILE";
                    setFragment(profileFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Fragments init
        homeFragment = new HomeFragment();
        orderFragment = new OrderFragment();
        profileFragment = new ProfileFragment();
        calendarFragment = new CalendarFragment();


    }

    @Override
    public void onResume(){
        super.onResume();
        // Set HOME when logsIn
        if (CustomGlobal.window == "HOME"){
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        // Set HOME when logOut is pressed
        if (CustomGlobal.isHome == true){
            CustomGlobal.window = "HOME";
        }
    }

    @Override
    public void onBackPressed(){
        if(CustomGlobal.window == "HOME"){
            moveTaskToBack(true);
        }else{
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_main, fragment);
        fragmentTransaction.commit();
    }

}
