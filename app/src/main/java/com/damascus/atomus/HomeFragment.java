package com.damascus.atomus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.damascus.atomus.Adapters.HomeAdapter;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;


public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        // Set window name
        CustomGlobal.window = "HOME";


        // Get recycler from R
        RecyclerView rvPractices = (RecyclerView) v.findViewById(R.id.home_cards);

        // Create linear manager to manage recycler's layout
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());


        // Get custom adapter
        HomeAdapter adapter = new HomeAdapter(CustomGlobal.practices);
        rvPractices.setHasFixedSize(true);

        // Set layout manager and set adapter
        rvPractices.setLayoutManager(linearLayoutManager);
        rvPractices.setAdapter(adapter);

        // Add dividers
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPractices.getContext(), linearLayoutManager.getOrientation());
        rvPractices.addItemDecoration(dividerItemDecoration);

        // Add recycler anims
        rvPractices.setItemAnimator(new SlideInUpAnimator());


        // Inflate the layout for this fragment
        return v;
    }

}
