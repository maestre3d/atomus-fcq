package com.damascus.atomus;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PreorderActivity extends AppCompatActivity implements View.OnClickListener {

    private static String course = "NULL", practice = "NULL";
    ArrayList<String> materials;
    OrderActivity orderActivity = new OrderActivity();

    // UI expressions
    TextView coursename, practicename;
    ListView materialView;
    Button post, files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.slidein_right, R.anim.slideout_left);

        setContentView(R.layout.activity_preorder);

        coursename = (TextView) findViewById(R.id.preorder_course);
        practicename = (TextView) findViewById(R.id.preorder_practice);
        post = (Button) findViewById(R.id.preorder_post);
        files = (Button) findViewById(R.id.preorder_adj);
        materialView = (ListView) findViewById(R.id.preorder_list);

        files.setOnClickListener(this);
        post.setOnClickListener(this);

        materials = new ArrayList<String>();
        materials.add("Matraz Herlenmeyer");
        materials.add("Aparato de destilacion");
        materials.add("Pinzas Mohr");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, materials);

        materialView.setAdapter(adapter);

        materialView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView args0, View view, int position, long id) {
                Toast.makeText(PreorderActivity.this, materials.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        coursename.setText(course);
        practicename.setText(practice);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
        finish();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.preorder_adj:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://quim.iqi.etsii.upm.es/didacticaquimica/actividades/Recursossantiago.pdf"));
                startActivity(browserIntent);
                break;
            case R.id.preorder_post:
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    builder.setMessage(Html.fromHtml( getString(R.string.infoAlert), Html.FROM_HTML_MODE_LEGACY))
                            .setTitle("¿Desea completar la orden?")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                                    orderActivity.setPractice(practicename.getText().toString());
                                    orderActivity.setTitle("Pedido");
                                    orderActivity.setCourse(coursename.getText().toString());
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }else{
                    builder.setMessage("Para confirmar su orden, favor de presionar el botón Aceptar.\n\n" + Html.fromHtml(getString(R.string.infoAlert)))
                            .setTitle("¿Desea completar la orden?")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                                    orderActivity.setPractice(practicename.getText().toString());
                                    orderActivity.setTitle("Pedido");
                                    orderActivity.setCourse(coursename.getText().toString());
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }

                break;
        }
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public void setPractice(String practice) {
        this.practice = practice;
    }
}
