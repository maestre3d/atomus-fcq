package com.damascus.atomus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.damascus.atomus.Models.Material;
import com.damascus.atomus.Models.Order;

import java.util.ArrayList;

public class OrderActivity extends AppCompatActivity {

    // Materials listview
    ListView listView;
    // Materials list
    ArrayList<Material> materials;

    // Avoid loop list flag
    Boolean flag = false;

    // Params
    static String practice = "NULL";
    static String course = "NULL";
    static String title = "Práctica";
    TextView pTx, mTx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set activity transition
        overridePendingTransition(R.anim.slidein_right, R.anim.slideout_left);


        setContentView(R.layout.activity_order);
        CustomGlobal.isHome = false;

        // Get R from activity
        pTx = (TextView) findViewById(R.id.dorder_in3);
        mTx = (TextView) findViewById(R.id.dorder_in1);
        listView = (ListView) findViewById(R.id.dorder_list);

        // Create a materials set once
        createMaterials();

        // Create an list adapter to insert using android's default list layout
        // Edited toString() method on model to get name from object
        ArrayAdapter<Material> materialAdapter = new ArrayAdapter<Material>(this, android.R.layout.simple_list_item_1, materials);
        listView.setAdapter(materialAdapter);

        // Set params
        pTx.setText(practice);
        mTx.setText(course);
        getSupportActionBar().setTitle(title);

        // listview listeners
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View v,int position, long arg3)
            {
                Material selectedmat=materials.get(position);
                Toast.makeText(getApplicationContext(), "Material seleccionado : " + selectedmat.getName(),   Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
        finish();
    }

    public void setPractice(String practice){
        this.practice = practice;
    }

    public void setCourse(String course){
        this.course = course;
    }

    public void setTitle(String title){ this.title = title; }

    private void createMaterials(){

        if(flag == false){
            materials = new ArrayList<Material>();

            for ( int i = 0; i < 5; i++ ){
                materials.add(new Material("Matraz " + i, "Matraz para herlen."));
            }

            flag = true;
        }
    }
}
